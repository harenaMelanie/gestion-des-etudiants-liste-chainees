/*____________RANAIVOARISON Harena Finaritra________________*/
/*_______________harenaharena77@gmail.com______________________*/
/*_____________________L3 MISA__________________________________*/

#include <iostream> 
#include <string>
#include <iostream>
#include <fstream> //bibliothèque standard pou ecrire des fichiers
#include "Ecole.hpp"

using namespace std;

int main(){

    cout << "-----------------------------------------------------------------------" << endl;
    cout << "\t\t\t Gestion des etudiants" << endl;
    cout << "-----------------------------------------------------------------------" << endl;
    bool done(false);
    Menu  mymenu;
    Lstudent listes; //déclaration d'un objet de type Lstudents

//Initialisations des données
    listes.initData();

//Initialisation du Menu
    mymenu.initMenu();
//Gestion des taches avec le menu
    while(!done){
        mymenu.getChoice();
        mymenu.manage(listes,done);
    }
     return 0;   
}

/*---Student--*/

Student::Student(){ //Constructeur de la classe student 
    this->name= name;
    this->notes = notes;
}

//Getters et setters de la classe student
string Student::getName(){
     return name;
}
float Student::getNotes(){
    return notes;
}
void Student::setName(string name){
    this->name = name;
}
void Student::setNotes(float notes){
    this->notes = notes;
}
//fonction qui affiche l'etudiant
void Student::display(){
    cout << "Nom:" <<name << endl;
    cout <<"Notes:" <<notes << endl;
}

//Destructeur de la classe student
 Student::~Student(){
}

/*--Element----*/

Element::Element(){ //constructeur de la classe element
    pnext =nullptr;
    
}
Element::~Element(){ //destructeur de la classe element

}

//getters Element
Student Element::getEleve(){
    return eleve;
}
Element* Element::getNext(){
    return pnext;
}
//setters Element
void Element::setEleve(Student& eleve){
    this->eleve = eleve;
}

void Element::setnext(Element* pnext){
    this->pnext = pnext;
}


/*---Gestion des listes etudiants---*/
Lstudent::Lstudent(){ 
    phead= nullptr;
}
Lstudent::~Lstudent(){
}
//getters and setters
void Lstudent::setPhead(Element *phead){
    this->phead = phead;
}
Element* Lstudent::getPhead(){
    return phead;
}

//Ajouter un etudiant dans la liste
void Lstudent::addStudent(Student eleve){
    Element* newElement = new Element;
    newElement->setEleve(eleve);
    newElement->setnext(phead);
    phead = newElement;
    readyForData = true;
}

//Supprimer une etudiant dans la liste
void Lstudent::delStudent(Student eleve){
    Element* pcur = getPhead();
    Element* pprev(nullptr);
    while(pcur != nullptr && pcur->getEleve().getName() != eleve.getName()){
        pprev = pcur;
        pcur = pcur->getNext();
    }
    //Si l'etudiant n'est pas dans la liste
    if(pcur == nullptr){
        cout << pcur->getEleve().getName() << "does'nt exists" << endl;
    }
    //Suppression de la premier de la liste
    if(pprev == nullptr){
        setPhead(pcur->getNext());
    }
    //Suppression au milieu de la liste
    else{
        pprev->setnext(pcur->getNext());
    }

    delete pcur;
}

//afficher la liste des etudiants
void Lstudent::displayList(){
    Element* pcur = phead;
    cout << "---------------------------------------------------" << endl;
    cout <<"\t\tVoici la liste des etudiants" << endl;
    cout << "---------------------------------------------------" << endl;
    while(pcur!=nullptr){
        cout <<"\t\t" << pcur->getEleve().getName() << "\t\t";
        cout <<"\t\t"<< pcur->getEleve().getNotes() << endl;
        pcur = pcur-> getNext();
    }
    cout <<"----------------------------------------------------" << endl;
}

//Initialiser data
void Lstudent::initData(){
    dataFile = ("fic.txt");
}

//sauver les donnees dans un fichier
 void Lstudent::saveData(string filename){
    ofstream outFile(filename); //Creation d'un flux d'entrée
    //inFile.open(dataFile);
    if(outFile.is_open()){
         Element* pcur;
         pcur = phead;
         if(pcur!=nullptr){
            while(pcur != nullptr){
                outFile << (pcur->getEleve()).getName();
                outFile << "\t\t";
                outFile << (pcur->getEleve()).getNotes();
                outFile << endl;  
                pcur = pcur->getNext();
            }
            cout << " Liste sauvegardé dans le fichier"  << "\t" << filename << endl;
         }
         else{
            cout << "La liste est vide, rien à suvegarder" << endl;
         }
    }

    outFile.close();
 }

//Recuperer les données
void Lstudent::recupData(string filename){
    ifstream inFile(filename);
    if(inFile.is_open()){
        string line;
        while(getline(inFile,line)){
             cout << "  " << line << endl;
        }
    }
    else{
        cout <<"le fichier" << filename << "est introuvable"<< endl;
    }
    inFile.close();
}

//getters read data;
bool Lstudent:: getReadData(){
    return readData;
}
void Lstudent::setReadData(bool readData){
    this->readData = readData;
}


bool Lstudent:: getReadyForData(){
    return readyForData;
}
void Lstudent::setReadyForData(bool readyForData){
    this->readyForData = readyForData;
}

//Gestion des Menu
Menu ::Menu(){
    dim = 6;
    items = nullptr;
    pst = nullptr;
    items = new (nothrow) string[dim];
    if(items != nullptr){
        items[0] = " 0  Quitter le programme";
        items[1] = "1  Afficher les listes des etudiants";
        items[2] = "2  Lire les donnees sur disques";
        items[3] = "3  Ajouter un etudiant";
        items[4] = "4  Sauver la liste des étudiants";
        items[5] = "5  Supprimer un etudiant";    }
    else{
        exit(1);
    }

}
Menu::~Menu(){
    delete [] items;
}

void Menu::initMenu(){
    Lstudent liste;
    liste.setReadData(false);
    choice = -1; 
}

int Menu::getChoice(){
    //display   menu
    cout << "Veuillez choisir:" << endl;
    for (int i(1); i<6; i++){
        cout <<"\t\t " << items[i] << endl;
    }
    cout << "\t\t" << items[0] << endl;

    cout << "Votre choix SVP:";
    cin >> choice;

    if(choice<0 || choice>6){
        getChoice();
    }
    return choice;
}
void Menu::manage(Lstudent& liste, bool &done){
    Student E1;
    string nomE1;
    float note;
    switch (choice){
        case 1:
            if(liste.getReadyForData()==true || readData == true){
                liste.displayList();
            }
            break;
        case 2:
            cout << "Voici les données dans le fichier" << endl;
            cout << "--------------------------------" << endl;
            cout << " Nom " << "\t |  \t" << " Notes " << endl;
            cout << "--------------------------------" << endl;
            liste.recupData("fic.txt");
            cout <<"---------------------------------" << endl;
            break;
        case 3:
            cout<< "Entrer le nom du nouvel etudiant:";
            cin >> nomE1;
            E1.setName(nomE1);
            cout<< "Entrer sa note:";
            cin >> note;
            E1.setNotes(note);
            liste.addStudent(E1);
            cout << "Un nouvel étudiant a été ajouté dans la liste" << endl; 
            cout << endl;
            break;
        case 4:
            liste.saveData("fic.txt");
            break;
        case 5:
            cout << "Entrer le nom de l'eleve à effacer:";
            cin >> nomE1;
            E1.setName(nomE1);
            cout<< "Entrer sa note:";
            cin >> note;
            E1.setNotes(note);
            liste.delStudent(E1);
            cout << "Etudiant supprimée"<< endl;;
            break;
        case 0:
            done = true;
            cout << "\n Merci à Vous et à plus!!! \n" << endl;
            break;
        default:
            break;
    }
   
}

