#include <iostream>
#include <string>

using namespace std;

//Creation de la classe Student
class Student{
    private:
        string name;
        float notes;
    public:
        Student();
        ~Student();
         string getName(); //permet d'acceder les variables membres en
         void setName(string name);
         float getNotes();
         void setNotes(float notes);
        void display(); //affichage de l'etudiant
       

};

//Creation d'une element dans la liste chainee
class Element{
    private:
        Student eleve;
        Element* pnext; //pointeur vers l'objet suivant
    public:
        Element();
        ~Element();
        Student getEleve();
        void setEleve(Student& eleve);
        Element* getNext();
        void setnext(Element* pnext);
        void displayElement();
};

//Creation de la classe 
class Lstudent{
   private:
       Student stud;
       Element *phead;
       bool readData;
       bool readyForData;
       string dataFile;
    public:
        Lstudent();
        ~Lstudent();
        Element* getPhead();
        bool getReadData();
        void setReadData(bool readData);
        bool getReadyForData();
        void setReadyForData(bool readyForData);
        void setPhead(Element *phead);
        void addStudent(Student eleve); //methode pour ajouter une etudiante dans la liste
        void delStudent(Student eleve); // methode pour supprimer une etudiante dans la liste
        void displayList(); //afficher la liste des étudiants
        void initData(); //initialiser les données
        void saveData(string filename); // sauver la liste dans un fichier
        void recupData(string filename); //recuperer les donnees 
 };     

 class Menu {
    public:
        Menu();// constructeur
        int getChoice();
        void manage(Lstudent& liste, bool &done);
        void initMenu();
        ~Menu();
    private: 
        int dim; // dimension di tableau
        string *items;
        int choice;
        Student *pst;
        bool readData;
};